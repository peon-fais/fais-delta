package main

import (
	"fmt"
	"net/http"

	"github.com/naoina/denco"
)

func index(w http.ResponseWriter, r *http.Request, params denco.Params) {
	fmt.Fprintf(w, "Welcome to delta!")
}
