package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"

	"github.com/naoina/denco"
)

var port = flag.Int("port", 8080, "port")

func main() {
	mux := denco.NewMux()
	handler, err := mux.Build([]denco.Handler{
		mux.GET("/", index),
	})
	if err != nil {
		panic(err)
	}
	log.Printf("starting on port : %d", *port)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", *port), handler))
}
